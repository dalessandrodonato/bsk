package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;

public class GameTest {
	
	@Test 
	//Test user story 3
	public void testGetScore() throws Exception{
		Frame frame=new Frame(1,4);
		Game game=new Game();
		game.addFrame(new Frame(4,3));
		game.addFrame(new Frame(1,5));
		game.addFrame(frame);
		game.addFrame(new Frame(6,3));
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(2,8));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(2,1));
		game.addFrame(new Frame(4,1));
		
		assertEquals(frame,game.getFrameAt(2));
	}
	
	@Test 
	//Test user story 4
	public void testCalculateScore() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		int score = 81;
		
		assertEquals(score,game.calculateScore());
	}
	
	@Test
	// Test for user story 5
	public void testCalculateScoreWithSpare() throws BowlingException {
		Game game = new Game();
		game.addFrame(new Frame(1,9));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		int score = 88;
		assertEquals(score, game.calculateScore());
	}

	
	
	@Test 
	//Test user story 6
	public void testCalculateStike() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));

		int score=94;
		
		assertEquals(score,game.calculateScore());
	}
	
	
	@Test 
	//Test user story 7
	public void testCalculateStikeAndSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(4,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		int score = 103;
		
		assertEquals(score,game.calculateScore());
	}
	
	
	
	@Test 
	//Test user story 8
	public void testCalculateMultipleStrike() throws Exception{
		
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		int score=112;
		
		assertEquals(score,game.calculateScore());

	}
	
	@Test 
	//Test user story 9
	public void testCalculateMultipleSpare() throws Exception{
		
		Game game = new Game();
		game.addFrame(new Frame(8,2));
		game.addFrame(new Frame(5,5));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,6));
		
		int score = 98;
				
		assertEquals(score, game.calculateScore());

	}
	
	@Test 
	//Test user story 10
	public void testCalculateScoreWithSpareLastFrame() throws Exception{
		
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(2,8));
		
		int score = 90;
		
		game.setFirstBonusThrow(7);
		
		assertEquals(score, game.calculateScore());

	}
	
	
	@Test 
	//Test user story 11
	public void testCalculateScoreWithStrikeAsLastFrame() throws Exception{
		
		Game game = new Game();
		game.addFrame(new Frame(1,5));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(7,2));
		game.addFrame(new Frame(3,6));
		game.addFrame(new Frame(4,4));
		game.addFrame(new Frame(5,3));
		game.addFrame(new Frame(3,3));
		game.addFrame(new Frame(4,5));
		game.addFrame(new Frame(8,1));
		game.addFrame(new Frame(10,0));
		
		int score = 92;
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(2);
		
		assertEquals(score, game.calculateScore());

	}
	
	@Test 
	//Test user story 12
	public void testCalculateScoreWithBestScore() throws Exception{
		
		Game game = new Game();
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		game.addFrame(new Frame(10,0));
		
		int score = 300;
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(score, game.calculateScore());

	}
	
	
	
}
