package tdd.training.bsk;

import static org.junit.Assert.*;

import org.junit.Test;


public class FrameTest {

	@Test 
	//Test user story 1
	public void testGetFirstThrow() throws Exception{
		int firstThrow=1;
		Frame frame=new Frame(1,3);
		assertEquals(firstThrow,frame.getFirstThrow());
	}
	
	@Test 
	//Test user story 1
	public void testGetFSecondThrow() throws Exception{
		int secondThrow=3;
		Frame frame=new Frame(1,3);
		assertEquals(secondThrow,frame.getSecondThrow());
	}
	
	@Test 
	//Test user story 2
	public void testGetScore() throws Exception{
		int score=4;
		Frame frame=new Frame(1,3);
		assertEquals(score,frame.getScore());
	}
	
	@Test 
	//Test user story 5
	public void testIsSpare() throws Exception{
		Frame frame=new Frame(4,6);
		assertTrue(frame.isSpare());
	}
	
	@Test 
	//Test user story 2
	public void testGetScoreWithSpare() throws Exception{
		int score=10;
		int bonus=2;
		Frame frame=new Frame(1,9);
		frame.setBonus(bonus);
		assertEquals(score+bonus,frame.getScore());
	}
	
}
