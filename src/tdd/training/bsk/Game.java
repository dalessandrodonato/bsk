package tdd.training.bsk;

import java.util.ArrayList;

public class Game {
	
	protected ArrayList<Frame> frames;
	protected int firstBonusThrow;
	protected int secondBonusThrow;
	
	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames=new ArrayList<Frame>(10);
		this.firstBonusThrow=0;
		this.secondBonusThrow=0;
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frames.add(frame);
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames.get(index);	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow=firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow=secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return this.firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return this.secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int score = 0;
		int bonus = 0;
		for(int i = 0; i < this.frames.size(); i++) {
			if(this.getFrameAt(i).isSpare() && i == 9) {
				bonus = this.getFirstBonusThrow();
				this.getFrameAt(i).setBonus(bonus);
			}
			if(this.getFrameAt(i).isSpare() && i != 9) {
				bonus =this.getFrameAt(i+1).getFirstThrow();
				this.getFrameAt(i).setBonus(bonus);
			}if(this.getFrameAt(i).isStrike() && i == 9) {
				bonus = this.getFirstBonusThrow() + this.getSecondBonusThrow();
				this.getFrameAt(i).setBonus(bonus);
			}
			if(this.getFrameAt(i).isStrike() && i == 8 && this.getFrameAt(9).isStrike()) {
				bonus = this.getFrameAt(i+1).getFirstThrow() + this.getFrameAt(i+1).getSecondThrow() + this.getFirstBonusThrow();
				this.getFrameAt(i).setBonus(bonus);
			}
			if(this.getFrameAt(i).isStrike() && i != 9 && i != 8) {
				if(this.getFrameAt(i+1).isStrike()) {
					bonus =this.getFrameAt(i+1).getFirstThrow() + this.getFrameAt(i+1).getSecondThrow() + this.getFrameAt(i+2).getFirstThrow();
					this.getFrameAt(i).setBonus(bonus);
				}
				else {
					bonus = this.getFrameAt(i+1).getFirstThrow() + this.getFrameAt(i+1).getSecondThrow();
					this.getFrameAt(i).setBonus(bonus);
				}
				
			}
			score = score + this.frames.get(i).getScore();
		}
		return score;

	}
}
